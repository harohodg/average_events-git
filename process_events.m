clear;

ROOT           = 'C:\Users\Student\Desktop\Harold_Hodgins\Zahra_2020\test_data\';
results_folder = '\averaged_results\';
results_folder = strcat(ROOT,results_folder);%Final files will be in ROOT\results_folder
global_average_results_folder = strcat(results_folder,'global_average_results\');%Global average results will be in ROOT\results_folder\global_average_results

events      = {'RCC','NW','RIC','RNC','UCC','UIC','UNC'};
time_slices =  [0 100; 100 200; 200 300; 300 400; 400 500; 500 600; 600 700; 700 800; 800 900];
excell_file        = 'Averaged_Events.xls';
global_excell_file = strcat('global_',excell_file);%Final excell file will be global_Averaged_Events.xls'

d = dir(ROOT);
isub = [d(:).isdir]; %% # returns logical vector
folders = {d(isub).name}';
folders(ismember(folders,{'.','..'})) = [];

%------------------------------------
[ALLEEG, EEG, ~, ~] = eeglab;

%For each folder in ROOT merge all *event_type*.set files, take the average of their timeslices and export to excell.
for folder_index = 1: numel(folders)
   current_folder_name = char( folders( folder_index ) );
   folder = strcat( current_folder_name, '\' );
   display( sprintf('Processing folder : %s',strcat(ROOT,folder) ) );
   
   try
       for k=1:length(events)
           current_event=events{k};
           current_excell_file = strcat(current_folder_name, '_',excell_file);
           current_set_file = strcat(current_folder_name,'_', current_event,'_merged.set');
           average_events(strcat(ROOT,folder), current_event, time_slices, results_folder,current_set_file, current_excell_file);
       end
   catch ME
       warning( 'Problem Processing folder : %s',strcat(ROOT,folder) );
       warning( ME.message );
   end
   
end

%For each event type do a global average.
fprintf('Performing global average');
try
   for k=1:length(events)
	   current_event=events{k};
	   current_set_file = strcat('global_', current_event,'_merged.set');
	   average_events(results_folder, current_event, time_slices, global_average_results_folder,current_set_file, global_excell_file);
   end
catch ME
   warning( 'Problem Processing folder : %s',strcat(ROOT,folder) );
   warning( ME.message );
end
