function export_to_excell(data_to_export, excell_file)
%Function to export data to excell
%Arguments :
%data_to_export : the data to export. A cell array of structs containing a label and a sheet.
%excell_file : The excell file to export to
%Returns Nothing.

warning('off','MATLAB:xlswrite:AddSheet');%turn off warning about new sheets
for sheet_index=1:numel(data_to_export)
	sheet_label = data_to_export(sheet_index).label;
	sheet_data  = data_to_export(sheet_index).sheet;
	
	fprintf('Exporting Results to sheet %s in file %s\n',sheet_label,excell_file);


	%Export excell data
	xlswrite( excell_file, sheet_data, sheet_label);
end