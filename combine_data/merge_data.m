function merged_data = merge_data(root_folder, folders, event_labels)
%Function to take a list of folders and event labels and return struct of merged data sets
%Arguments :
%root_folder : where are the folders located eg 'C:\Users\Student\Desktop\Harold_Hodgins\Zahra_2020\test_data\raw_data\'
%folders   : which folders to process. eg {'folder_1', 'folder_2'}
%event_labels : which event labels
%Returns merged_data : an array of structs
%	where each struct has two fields :
%		foldername  : the name of the folder that was processed
%		events_data : array of structs containing the events data with two fields
%			event_label : the event type (eg. RCC)
%			event_data  : the actual data as loaded by eeglab


%Go through every folder and merge all the data into one giant dataset.
for folder_index = 1: numel(folders)
	current_folder_name = char( folders( folder_index ) );
	current_folder_path = strcat(root_folder, current_folder_name,'\');
	display( sprintf('Processing folder : %s',current_folder_path ) );

	
	%merged_data.data_sets.(current_folder_name) = struct;
	
	current_folder_data             = struct;
	current_folder_data.foldername  = current_folder_name;
	%current_folder_data.events_data = struct;
	try
		for event_index=1:length(event_labels)
			current_event_data = struct;
            try
                event_label = event_labels{event_index};
				current_event_data.event_label = event_label;

                set_files  = dir( strcat(current_folder_path,'*',event_label,'*.set') );%Get list of set files
                file_names = { set_files.name };%Get file names

                raw_data   = pop_loadset('filename',file_names,'filepath',current_folder_path);%Load the files into EEGLAB
                
                if size(raw_data, 2) ~= 0
                    current_event_data.event_data = pop_mergeset(raw_data, linspace(1,numel(raw_data), numel(raw_data) ), 0);%Merge data sets and save.
                    %current_dataset.data.(event_label).data is now a 3D data set with (rows=channels, columns = time, 3rd dimension = each set file )
                
                    current_folder_data.events_data(event_index) = current_event_data;
                else
                    warning('%s has no %s data in it', current_folder_path, event_label);
                end
            catch ME
                warning( 'Problem merging %s data in %s', event_label, current_folder_path );
                warning( ME.message );
            end
			
        end
	catch ME
		warning( 'Problem Processing folder : %s', current_folder_path );
		warning( ME.message );
	end
	merged_data(folder_index) = current_folder_data;
end
   