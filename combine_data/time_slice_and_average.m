function time_sliced_data = time_slice_and_average(merged_data, time_slices)
%Function to take a list of folders and event labels and return struct of merged data sets
%Arguments :
%merged_data : the data to slice and average (see merge_data.m for format)
%time_slices : the time slices as a 2D matrix. each row is a slice. Column 1 is the start time in ms, second column is the end in ms
%Returns a struct with two fields 
%	merged_data : The original data but with an additional field in the event data struct for the timesliced data
%	time_slices : The 2D matrix used to do the timeslicing

time_sliced_data.merged_data = merged_data;
time_sliced_data.time_slices = time_slices;

folder_names = { merged_data.foldername };
for folder_name_index = 1: numel(folder_names)
	current_folder_name = char( folder_names( folder_name_index ) );
	
	try
		current_folder_data = merged_data(folder_name_index);
		event_names  = { current_folder_data.events_data.event_label };
		for event_index=1:length(event_names)
			event_label = char( event_names( event_index ) );
			display( sprintf('Time Slicing data for %s:%s',current_folder_name, event_label) );
            try
				current_event_data = current_folder_data.events_data(event_index).event_data;
				num_time_slices    = size(time_slices,1);
				time_data = zeros(current_event_data.nbchan, num_time_slices);
				
			
				time_index = 1;
				for r = 1:num_time_slices
					time_slice_row = time_slices(r,:);
					time_slice_start = time_slice_row(1);
					time_slice_end   = time_slice_row(2);
					fprintf('Extracting %f to %f ms\n',time_slice_start, time_slice_end );
					time_slice = pop_select( current_event_data, 'time', [time_slice_start/1000 time_slice_end/1000]);
					fprintf('Extracted %f to %f seconds\n\n',time_slice.xmin, time_slice.xmax);
					
					    trials_average = mean( time_slice.data, 3);
						time_average   = mean( trials_average, 2);
					
					time_data(:,time_index) = time_average;
					time_index = time_index + 1;
				end	
				
				current_folder_data.events_data(event_index).time_sliced_data = time_data;
            catch ME
				dbstack
                warning( 'Problem time slicing the data for %s: %s', current_folder_name, event_label );
                warning( ME.message );
            end
        end
	catch ME
		warning( 'Problem time slicing  the data for folder : %s', current_folder_name );
		warning( ME.message );
	end
    time_sliced_data.merged_data(folder_name_index) = current_folder_data;
end