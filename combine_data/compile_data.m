function compiled_data = compile_data(time_sliced_data, event_labels, channel_labels)
%Function to take time sliced data and compile it for export to Excell
%Arguments :
%time_sliced_data : the data to compile (see time_slice_and_average.m for format)
%event_labels     : the event labels to export
%channel_labels   : the channel labels to export
%Returns compiled_data : the data ready for export.

data_to_export = struct;


time_slices     = time_sliced_data.time_slices;
num_time_slices = size(time_slices,1);


%For every sheet
for r = 1:num_time_slices
	time_slice_row = time_slices(r,:);
	time_slice_start = time_slice_row(1);
	time_slice_end   = time_slice_row(2);
	
	current_sheet = compile_excell_sheet(event_labels, channel_labels, r, time_sliced_data);
	current_sheet_label = sprintf('%0.2f - %.2f ms', time_slice_start,time_slice_end);
	data_to_export(r).label = current_sheet_label;
	data_to_export(r).sheet = current_sheet;
	
end

compiled_data = data_to_export;