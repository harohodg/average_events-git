function header=get_header(event_label, channel_labels, other_labels)
%Function to generate excell sheet header from event_label, channel labels and other labels
%Arguments :
%event_label    : The event label to append to each channel label (eg. RCC)
%channel_labels : The channel labels to use
%other_labels   : The other labels to use
%Returns header : channel labels with event label concatentated onto the end followed by other labels with event label concatentated to the end
channel_header_labels = strcat(channel_labels, ':', event_label);
other_header_labels   = strcat(other_labels, ':', event_label);
header = [channel_header_labels, other_header_labels];