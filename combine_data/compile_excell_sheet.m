function sheet=compile_excell_sheet( event_labels, channel_labels, time_slice_index, time_sliced_data )
%Function to compile a single excell sheet
%Arguments :
%event_labels   : The event labels to use
%channel_labels : The channel labels to use
%time_slice_index : Which time slice to use 
%time_sliced_data : The dataset to compile from (see time_slice_and_average.m for format)
%Returns sheet    : A cell array with the data to put in one excell sheet


OTHER_LABELS   = { 'VM', 'LA', 'LP', 'RA', 'RP' };

%compile header row
header_row = cell(1,1);
for event_index=1:length(event_labels)
	event_label    = char( event_labels(event_index) );
	current_header = get_header(event_label, channel_labels, OTHER_LABELS);
	
	header_row = [header_row, current_header];
end

sheet = header_row;

%Iterate through each folder, event type, and channel label
merged_data = time_sliced_data.merged_data;
folder_names = { merged_data.foldername };
for folder_name_index = 1: numel(folder_names)
	try
		current_folder_name = char( folder_names( folder_name_index ) );
		current_folder_data = merged_data(folder_name_index);
		event_names  = { current_folder_data.events_data.event_label };
		
		current_row = {current_folder_name};
		
		for event_index=1:length(event_labels)
			event_label        = event_labels(event_index);
			target_event_index = find(strcmp(event_names, event_label), 1);
			
			if size(target_event_index,2) == 0%current folder doesn't contain this event type
				data_to_insert = cell(1, numel(channel_labels) + numel(OTHER_LABELS));
			else
				current_event_data = current_folder_data.events_data(target_event_index).time_sliced_data;
				current_data       = current_event_data(:,time_slice_index);
				channel_data       = cell(1, size(channel_labels,2) );
				other_data         = calculate_other(current_data);
				
				current_data_channels = { current_folder_data.events_data(target_event_index).event_data.chanlocs.labels };
				%Now extract the individual channels data and put in the correct loction.
				for channel_index=1:length(channel_labels)
					channel_label        = channel_labels(channel_index);
					target_channel_index = find(strcmp(current_data_channels, channel_label), 1);
					if size(target_channel_index,2) ~= 0%The channel label exists in this data set
						channel_data(channel_index) = num2cell( current_data(target_channel_index) );
					end
				end
				data_to_insert = [channel_data, num2cell( other_data ) ];
			end
			current_row = [current_row, data_to_insert];
        end
		sheet = [sheet;current_row];
	catch ME
		warning( 'Problem compiling excell data for folder : %s', current_folder_name );
		warning( ME.message );
	end
   
end

