function export_merged_data(merged_data, merged_data_folder)
%Function to take a list of folders and event labels and return struct of merged data sets
%Arguments :
%merged_data : the data to export (see merge_data.m for format)
%merged_data_folder   : where to put the data
%Returns nothing

display('Exporting merged data');

% %check for output folder
if ~isdir(merged_data_folder)
   mkdir(merged_data_folder) 
end


folder_names = { merged_data.foldername };
for folder_name_index = 1: numel(folder_names)
	current_folder_name = char( folder_names( folder_name_index ) );
	current_folder_path = strcat(merged_data_folder, current_folder_name,'\');
	display( sprintf('Exporting merged data to %s',current_folder_path ) );

	%check for output folder
	if ~isdir(current_folder_path)
	   mkdir(current_folder_path) 
	end
	
	try
		current_folder_data = merged_data(folder_name_index);
		event_names  = { current_folder_data.events_data.event_label };
		for event_index=1:length(event_names)
			event_label        = char( event_names( event_index ) );
			current_event_data = current_folder_data.events_data(event_index);
            try
				set_file_name  = strcat(current_folder_name,'_',event_label,'.set');
                data_to_export = current_event_data.event_data;
				pop_saveset( data_to_export, 'filename', set_file_name, 'filepath', current_folder_path,'savemode','onefile' );
            catch ME
                warning( 'Problem exporting %s data in %s', event_label, current_folder_path );
                warning( ME.message );
            end
        end
	catch ME
		warning( 'Problem exporting data for folder : %s', current_folder_path );
		warning( ME.message );
	end
   
end