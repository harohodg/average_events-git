function results = calculate_other(channel_data)
%Function to take channel data and calculate VM, LA, LP, RA, RP data
%Arguments :
%channel_data : the channels to average
%Returns results : vector of results for VM, LA, LP, RA, RP

mapping = {[ 1,20,15,14,4,3]; ...
          [30,29,27,25,26];...
          [18,13,11,12,2];...
          [28,22,23,16,17];...
          [6,9,5,7,1]};
	 

results = zeros(1,5);
for i = 1:5
	channels = cell2mat( mapping(i) );
	running_total = 0;
	for j = 1:numel(channels)
		channel = channels(j);
		running_total = running_total + channel_data(channel);
	end
	results(i) = running_total / numel(channels);
end
