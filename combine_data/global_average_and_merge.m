clear;

ROOT_FOLDER = 'C:\Users\Student\Desktop\Harold_Hodgins\Zahra_2020\test_data\raw_data\';
EVENTS      = {'RCC','NW','RIC','RNC','UCC','UIC','UNC'};
CHANNELS    = {'O2','O1','OZ','PZ','P4','CP4','P8','C4','TP8','T8','P7','P3','CP3','CPZ','CZ','FC4','FT8','TP7','C3','FCZ','FZ','F4','F8','T7','FT7','FC3','F3','FP2','F7','FP1','HEO','VEO'};
MERGED_DATA_FOLDER = 'C:\Users\Student\Desktop\Harold_Hodgins\Zahra_2020\test_data\merged_data\';
TIME_SLICES =  [0 100; 100 200; 200 300; 300 400; 400 500; 500 600; 600 700; 700 800; 800 900];
EXCELL_FILE = 'C:\Users\Student\Desktop\Harold_Hodgins\Zahra_2020\test_data\merged_averaged_sliced.xlsx';

%Initialize eeglab
[ALLEEG, EEG, ~, ~] = eeglab;

%Get list of folders to process
d = dir(ROOT_FOLDER);
isub = [d(:).isdir]; %% # returns logical vector
folders = {d(isub).name}';
folders(ismember(folders,{'.','..'})) = [];

%Go through every folder and merge all the data into one giant dataset.
merged_data =  merge_data(ROOT_FOLDER,folders, EVENTS);


%Now export the merged data
export_merged_data(merged_data, MERGED_DATA_FOLDER);

%Time slice and average the merged data
time_sliced_data = time_slice_and_average(merged_data, TIME_SLICES);


%Prepare data for export
data_to_export = compile_data(time_sliced_data, EVENTS, CHANNELS);

%And finally export data
export_to_excell(data_to_export, EXCELL_FILE);

