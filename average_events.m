function average_events(root_directory,event_label,time_slices,out_folder,set_file_name,excell_file_name)
%Function to take a series of eeglab .set files from a folder, average the
%results together by time and trials and store the result in an excell spread sheet 
%Arguments :
%root_directory : where are the files? eg. 'C:\Users\Student\Desktop\Zahra_data\4\'
%event_label : which event are we averageing? eg. 'RCC'
%time_slices : 2D matrix of time slices to extract. First column is the start time in ms (eg 100), second column is the end time in ms (eg 200)
%out_folder  : folder to put the results in. eg 'C:\\Users\\Student\\Desktop\\Zahra_data\\results\'
%set_file_name    : The name of the exported set file. This set file is just all the set files merged. 
%excell_file_name : The name of the excell file to put the results in. eg. 'Averaged_events.xls'
%Example usage
%average_events('C:\Users\Student\Desktop\Zahra_data\4\','RCC',time_slices'\results\','4_RCC_merged.set','Averaged_events.xls')
%where time_slices =  [0 100; 100 200; 200 300; 300 400; 400 500; 500 600; 600 700; 700 800; 800 900];
 
%check for output folder
if ~isdir(out_folder)
   mkdir(out_folder) 
end


[ALLEEG, EEG, ~, ~] = eeglab;%Initialize eeglab


files = dir( strcat(root_directory,'*',event_label,'*.set') );%Get list of set files
file_names = { files.name };%Get file names

raw_data = pop_loadset('filename',file_names,'filepath',root_directory);%Load the files into EEGLAB

merged_data = pop_mergeset(raw_data, linspace(1,numel(raw_data), numel(raw_data) ), 0);%Merge data sets
%We now have a 3D data set in merged_data.data (rows=channels, columns =
%time, 3rd dimension = event )




num_time_slices    = size(time_slices,1);
time_averaged_data = zeros(merged_data.nbchan, num_time_slices);


time_index = 1;
for r = 1:num_time_slices
    time_slice_row = time_slices(r,:);
	time_slice_start = time_slice_row(1);
	time_slice_end   = time_slice_row(2);
    fprintf('Extracting %f to %f ms\n',time_slice_start, time_slice_end );
    time_slice = pop_select( merged_data, 'time', [time_slice_start/1000 time_slice_end/1000]);
    fprintf('Extracted %f to %f seconds\n\n',time_slice.xmin, time_slice.xmax);
    
    trials_average = mean( time_slice.data, 3);
    time_average   = mean( trials_average, 2);
    
    time_averaged_data(:,time_index) = time_average;
    time_index = time_index + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %Export data to a new set file (only averaged across trials, not across time)
% %Create empty place holder set to put new data in   
 averaged_set = merged_data;
% 
 averaged_set.setname = strcat(event_label, '_merged');
% averaged_set.trials = 1;
% averaged_set.epoch  = [];
% %averaged_set.event  = [];
% averaged_set.filename = '';
% averaged_set.filepath = '';
% averaged_set.history = '';
% averaged_set.datfile = '';
% averaged_set.comments = char(strcat('Averaged ',event_label, 'files set') );
% 
% %Average the n files together
% averaged_set.data = mean(averaged_set.data,3);

%And save it to disk
pop_saveset( averaged_set, 'filename',set_file_name,'filepath',out_folder,'savemode','onefile' );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


excell_table = cell( merged_data.nbchan+1, num_time_slices+1);
%Add channels rows header
excell_table(2:size(excell_table,1),1) = {merged_data.chanlocs.labels};

%Add times header
time_index = 2;
for r = 1:num_time_slices
    time_slice_row = time_slices(r,:);
	time_slice_start = time_slice_row(1);
	time_slice_end   = time_slice_row(2);
    slice = sprintf('%0.2f - %.2f ms', time_slice_start, time_slice_end);
    excell_table(1,time_index) = cellstr(slice);
    time_index = time_index +1;
end

%insert numeric data
excell_table(2:size(excell_table,1), 2:size(excell_table,2)) = num2cell(time_averaged_data);


%Add VM, LA, LP, RA, RP data
other=[struct('label','VM','channels',[21,20,15,14,4,3]),...
     struct('label','LA','channels',[30,29,27,25,26]),...
     struct('label','LP','channels',[18,13,11,12,2]),...
     struct('label','RA','channels',[28,22,23,16,17]),...
     struct('label','RP','channels',[6,9,5,7,1])];


second_table = cell( numel(other), size(excell_table,2)); 
 
%compute VM, LA, etc
for s_index = 1 :numel(other)
   other_details = other(s_index);
   
   %Insert header
   second_table(s_index,1) = {other_details.label};
   
   %For each time slice
   for t_index = 1:size(time_averaged_data,2)
      values_to_average = zeros(1, numel(other_details.channels) );     
      for c_index = 1:numel(other_details.channels)
         channel_value = time_averaged_data(other_details.channels(c_index), t_index );
         values_to_average(c_index) = channel_value;
      end
      %store value
      m = mean(values_to_average,2);
      second_table(s_index, t_index+1) = {m};
   end

end
full_table = [excell_table; second_table];

%Do the actual export
warning('off','MATLAB:xlswrite:AddSheet');%turn off warning about new sheets
fprintf('Exporting Results to sheet %s in file %s\n',event_label,excell_file_name);


%Export excell data
xlswrite( strcat(out_folder,excell_file_name), full_table, event_label);

